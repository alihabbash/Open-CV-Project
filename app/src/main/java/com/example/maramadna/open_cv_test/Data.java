package com.example.maramadna.open_cv_test;

import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;

import java.util.ArrayList;

/**
 * Created by X_PC on 7/30/2017.
 */
public class Data {
    static int CircleRadius=7;

    static int DrawingThickness=4;
    static Point CentralOfRect=new Point(0,0);
    static Mat ContoursHeirarcy=new Mat();
    static Rect BoundingRectangle=new Rect();
    static MatOfInt ConvexHull=new MatOfInt();
    static MatOfInt4 ConvexityDefects=new MatOfInt4();
    static ArrayList<MatOfPoint> Contours=new ArrayList<>();

    static Scalar ContourColor=new Scalar(255,0,0);
    static Scalar StartPointColor=new Scalar(0,255,0);
    static Scalar DefectPointColor=new Scalar(0,0,255);
    static Scalar ConvexHullColor=new Scalar(0,255,255);
    static Scalar RectangleColor=new Scalar(255,0,255);

    static Scalar LowValuesForTH=new Scalar(44,137,100);
    static Scalar HighValuesForTH=new Scalar(180,255,255);
}
