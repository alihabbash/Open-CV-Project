package com.example.maramadna.open_cv_test;

/**
 * Created by ali on 8/3/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import org.opencv.core.Mat;
import java.util.ArrayList;

public class Calibrate {

    private ArrayList<Finger> Fingers;

    private SQLiteDatabase usersDB;

    private static final String TABLE_NAME = "USERS_FINGERS";

    private static final String USER_COLUMN = "USER_NAME";

    private static final String INDEX_COLUMN = "INDEX_FINGER_LENGTH";

    private static final String MIDDLE_COLUMN = "MIDDLE_FINGER_LENGTH";

    /**
     * Constructor that accept matrix of pixels
     * to write to it & the context
     * @param fingers Array list of fiingers object
     * @param context The context of your app
     */

    public Calibrate(ArrayList < Finger > fingers , Context context){

        UsersDatabase usersDBheleper = new UsersDatabase(context);

        usersDB =  usersDBheleper.getWritableDatabase();

        Fingers = fingers;

    }

    /**
     *Method to close the database connection
     * @throws Throwable Throwable exception to indicate there is an error in finish
     */

    void Close() throws Throwable {
      usersDB.close();
      this.finalize();
    }

    /**
     * Method to calibrate & record the length
     * of middle & index finger to database
     * @param name The username that is calibrating
     */

    public boolean Calibrating(String name) {

        int maxfingerlengthindex = GetMaxIndexLenght();

        Finger middlefinger = Fingers.get(maxfingerlengthindex);

        Finger indexfinger = Fingers.get(maxfingerlengthindex + 1);

        ContentValues lengthvalues = new ContentValues();

        lengthvalues.put(USER_COLUMN , name);

        lengthvalues.put(INDEX_COLUMN , indexfinger.GetFingerLenght());

        lengthvalues.put(MIDDLE_COLUMN , middlefinger.GetFingerLenght());

        long rowId = usersDB.insert("USERS_FINGERS",null,lengthvalues);

        return rowId != -1;

    }


    /**
     * Method to get the lenght of middle & index
     * finger for the specified user
     * @param name The username that require the values
     * @return Array list consist of index finger length
     *         cross bond to index 0 & middle finger length
     *         cross bond to index 1
     */

    public ArrayList<Float> getResult( String name ) {

        float indexlength , middlelength;

         Cursor selectResult = usersDB.query(TABLE_NAME,
                      new String[] {INDEX_COLUMN,MIDDLE_COLUMN},
                      USER_COLUMN + " = ?",
                        new String []{name},
                        null , null , null);

         indexlength = selectResult.getFloat(selectResult.getColumnIndex(INDEX_COLUMN));

         middlelength = selectResult.getFloat(selectResult.getColumnIndex(MIDDLE_COLUMN));

         ArrayList<Float> lengthslist = new ArrayList<>();

         lengthslist.add(indexlength);

         lengthslist.add(middlelength);

         return lengthslist;

    }

    /**
     * Method to get the longest finger's index
     * @return The index of the longest finger
     */

    private int GetMaxIndexLenght()
    {
        double Max = 0;

        int MaxIndex = 0;

        for(int i = 0 ; i < Fingers.size() ; i++)
        {
            if(Fingers.get(i).GetFingerLenght() > Max)
            {

                Max = Fingers.get(i).GetFingerLenght();

                MaxIndex = i;

            }

            Fingers.get(i).SetFingerName("Middle");

            Fingers.get( i + 1 ).SetFingerName("Index");
        }

        return MaxIndex;
    }

}
