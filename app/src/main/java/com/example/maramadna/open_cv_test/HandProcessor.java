package com.example.maramadna.open_cv_test;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by X_PC on 7/30/2017.
 */
public class HandProcessor {
    int[] ConvexIntHelper;
    Point[] BiggestContourPts;
    int BiggestContourIndex=-1;
    double MaxContourArea;
    double TempContourArea;
    double Distance;
    Mat BinaryImage=new Mat();
    Mat RGBAImage=new Mat();
    Mat YCRCBImage=new Mat();
    double Scale;
    boolean Draw;
    static int FrameCounts = 0;
    MatOfPoint ConvexHullMat;
    MatOfPoint2f MAT2F;
    MatOfPoint2f MAT2F2;
    List<Integer> ConvexityDefectList;
    ArrayList<Point> ConvexHullPoints=new ArrayList<>();
    ArrayList<Point> DefectPoints=new ArrayList<>();
    private ArrayList<Finger> Fingers=new ArrayList<>();

    Point StartPoint;
    Point EndPoint;
    Point DefectPoint;

    int StartCentralLenght;

//-----------------------------------------------
//-----------------Methods------------------------
//-----------------------------------------------

    public HandProcessor(boolean Draw) {
        ConvexHullPoints = new ArrayList<>();
        ConvexHullMat = new MatOfPoint();
        MaxContourArea = 0;
        TempContourArea = 0;
        this.Draw = Draw;
        Scale = 0.2;
    }

    public ArrayList<Finger> BeginProcess(Mat binaryImage) {
        InitVariables();
        ConvertImage();
        FindContour();
        FindBiggestContour();
        if (BiggestContourIndex != -1) {
            FindConvexHull();
            FindConvexityDefects();
            FindRectangle();
            FindFingers();
            if (Draw) {
                DrawShapes();
            }
        }
        BinaryImage = binaryImage;
        return Fingers;
    }

    private void InitVariables()
    {
        MaxContourArea=0;
        TempContourArea=0;
        DefectPoints.clear();
        ConvexHullPoints.clear();
        BiggestContourIndex=-1;
        Data.Contours.clear();
        Fingers.clear();
    }

    private void ConvertImage() {
        Imgproc.cvtColor(RGBAImage, RGBAImage, Imgproc.COLOR_RGBA2RGB);
        Imgproc.cvtColor(RGBAImage, YCRCBImage, Imgproc.COLOR_RGB2YCrCb);
        Core.inRange(YCRCBImage, Data.LowValuesForTH, Data.HighValuesForTH, BinaryImage);
    }

    private void FindContour() {
        Imgproc.findContours(BinaryImage, Data.Contours, Data.ContoursHeirarcy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE);
    }

    private void FindBiggestContour() {
        for (int i = 0; i < Data.Contours.size(); i++) {
            TempContourArea = Data.Contours.get(i).size().area();
            if (TempContourArea > MaxContourArea) {
                MaxContourArea = TempContourArea;
                BiggestContourIndex = i;
            }
        }
    }

    private void FindConvexHull() {
        MAT2F = new MatOfPoint2f(Data.Contours.get(BiggestContourIndex).toArray());
        Distance = Imgproc.arcLength(MAT2F, true) * Scale;
        MAT2F2 = new MatOfPoint2f();
        Imgproc.approxPolyDP(MAT2F, MAT2F2, Distance, true);
        Imgproc.convexHull(Data.Contours.get(BiggestContourIndex), Data.ConvexHull);
    }

    private void FindConvexityDefects() {
        ConvexHullPoints.clear();
        ConvexIntHelper = Data.ConvexHull.toArray();

        for (int i = 0; i < ConvexIntHelper.length; i++) {
            ConvexHullPoints.add(Data.Contours.get(BiggestContourIndex).toList().get(ConvexIntHelper[i]));
        }
        ConvexHullMat.fromList(ConvexHullPoints);
        Imgproc.convexityDefects(Data.Contours.get(BiggestContourIndex), Data.ConvexHull, Data.ConvexityDefects);
    }

    private void FindRectangle() {
        Data.BoundingRectangle = Imgproc.boundingRect(Data.Contours.get(BiggestContourIndex));
    }

    private void FindFingers() {
        double Angle;
        double InAngle;

        Fingers.clear();
        ConvexityDefectList = Data.ConvexityDefects.toList();
        BiggestContourPts = Data.Contours.get(BiggestContourIndex).toArray();
        Data.CentralOfRect = new Point(Data.BoundingRectangle.x + Data.BoundingRectangle.width / 2,
                Data.BoundingRectangle.y + Data.BoundingRectangle.height / 2);

        for (int i = 0; i < ConvexityDefectList.size(); i = i + 4) {
            StartPoint = BiggestContourPts[ConvexityDefectList.get(i)];
            EndPoint = BiggestContourPts[ConvexityDefectList.get(i + 1)];
            DefectPoint = BiggestContourPts[ConvexityDefectList.get(i + 2)];

            StartCentralLenght = GetLength(StartPoint, Data.CentralOfRect);

            Angle = Math.atan2(Data.CentralOfRect.y - StartPoint.y, Data.CentralOfRect.x - StartPoint.x) * 180 / Math.PI;
            InAngle = innerAngle(StartPoint.x, StartPoint.y, EndPoint.x, EndPoint.y, DefectPoint.x, DefectPoint.y);
            if (IsFinger(Angle,InAngle)) {
                Fingers.add(new Finger(GetLength(StartPoint,DefectPoint)));
                DefectPoints.add(DefectPoint);
            }
        }
    }

    private int GetLength(Point Point1, Point Point2) {
        return (int) Math.sqrt(Math.pow((Point1.x - Point2.x), 2) + Math.pow((Point1.y - Point2.y), 2));
    }

    double innerAngle(double px1, double py1, double px2, double py2, double cx1, double cy1) {

        double dist1 = Math.sqrt((px1 - cx1) * (px1 - cx1) + (py1 - cy1) * (py1 - cy1));
        double dist2 = Math.sqrt((px2 - cx1) * (px2 - cx1) + (py2 - cy1) * (py2 - cy1));

        double Ax, Ay;
        double Bx, By;
        double Cx, Cy;

        Cx = cx1;
        Cy = cy1;
        if (dist1 < dist2) {
            Bx = px1;
            By = py1;
            Ax = px2;
            Ay = py2;


        } else {
            Bx = px2;
            By = py2;
            Ax = px1;
            Ay = py1;
        }

        double Q1 = Cx - Ax;
        double Q2 = Cy - Ay;
        double P1 = Bx - Ax;
        double P2 = By - Ay;

        double A = Math.acos((P1 * Q1 + P2 * Q2) / (Math.sqrt(P1 * P1 + P2 * P2) * Math.sqrt(Q1 * Q1 + Q2 * Q2)));
        A = A * 180 / Math.PI;
        return A;
    }

    private boolean IsFinger(double Angle,double InAngle)
    {
        return Angle > -30 && Angle < 160 && Math.abs(InAngle) > 20 && Math.abs(InAngle) < 120 && StartCentralLenght > 0.1 * Data.BoundingRectangle.height;
    }

    private void DrawShapes()
    {
        Data.Contours.add(ConvexHullMat);
        Imgproc.cvtColor(BinaryImage,BinaryImage,Imgproc.COLOR_GRAY2RGBA);

        Imgproc.drawContours(BinaryImage,Data.Contours,BiggestContourIndex,Data.ContourColor,Data.DrawingThickness);
        Imgproc.drawContours(BinaryImage,Data.Contours,Data.Contours.size()-1,Data.ConvexHullColor,Data.DrawingThickness);
        Imgproc.rectangle(BinaryImage,Data.BoundingRectangle.tl(),Data.BoundingRectangle.br(),Data.RectangleColor,Data.DrawingThickness);

        for(int i=0 ; i<Fingers.size() ; i++)
            Imgproc.circle(BinaryImage,Fingers.get(i).GetFingerTipPoint(),Data.CircleRadius,Data.StartPointColor,Data.CircleRadius*2);

        for(int i=0 ; i<DefectPoints.size() ; i++)
            Imgproc.circle(BinaryImage,DefectPoints.get(i),Data.CircleRadius,Data.DefectPointColor,Data.CircleRadius*2);

        Imgproc.circle(BinaryImage,Data.CentralOfRect,Data.CircleRadius,Data.DefectPointColor,Data.CircleRadius*2);
    }
}
