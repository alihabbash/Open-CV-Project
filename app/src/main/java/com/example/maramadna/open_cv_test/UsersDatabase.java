package com.example.maramadna.open_cv_test;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UsersDatabase extends SQLiteOpenHelper {

    private static final String DB_NAME = "LoginDB";

    private static final int DB_VERSION = 1;

    /**
     * Constructor that initialize the database
     * @param context The context object
     */

    public UsersDatabase(Context context){

        super(context , DB_NAME , null , DB_VERSION);

    }

    /**
     * The onCreate method is called when the database is created
     * that create the required tables
     * @param db The database object
     */

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sqlCommend = "CREATE TABLE USERS_FINGERS (" +
                                "_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                                "USER_NAME TEXT UNIQUE," +
                                "INDEX_FINGER_LENGTH REAL," +
                                "MIDDLE_FINGER_LENGTH REAL);";

        db.execSQL(sqlCommend);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


}
