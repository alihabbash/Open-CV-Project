package com.example.maramadna.open_cv_test;

import android.content.Context;
import android.graphics.Point;
import android.widget.Toast;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by X_PC on 8/3/2017.
 */
public class MovementProcessor {

    int SecondToWait;

    Context MyContext;

    Process RootProcess;

    DataOutputStream RootOutputStream;

    Finger Middle,Index;

    boolean MiddleFound,IndexFound;

    Point FirstIndexPoint;

    private static int swipFrameCount = 0;

    public MovementProcessor(Context MyContext,int Seconds)
    {
        FirstIndexPoint=new Point(0,0);
        this.MyContext=MyContext;
        SecondToWait=Seconds;
        MiddleFound=false;
        IndexFound=false;

        try {
            RootProcess = Runtime.getRuntime().exec("su");
            RootOutputStream=new DataOutputStream(RootProcess.getOutputStream());
        }
        catch(IOException E)
        {
            Toast.makeText(this.MyContext,"Error getting root",Toast.LENGTH_SHORT).show();
        }
    }

    public void PerformActionIfRequired(ArrayList<Finger> Fingers,int CircleRadius,int FrameCount)
    {
        Middle=null;
        Index=null;
        IndexFound=false;
        MiddleFound=false;

        FindMiddleOrIndex(Fingers);
        if(!MiddleFound && !IndexFound)
            return;
        if(MiddleFound && !IndexFound)
            return;
        if(!MiddleFound && IndexFound)
            ProcessTap(CircleRadius,FrameCount);
        else
            ProcessSwip();
    }
    private void ClickOnScreen(double X,double Y)
    {
        try
        {
            RootOutputStream.writeBytes("input tap "+X+" "+Y+"\n");
            RootOutputStream.flush();
            RootOutputStream.writeBytes("exit\n");
            RootOutputStream.flush();
        }
        catch(IOException E)
        {
            Toast.makeText(MyContext,"Error while writing command",Toast.LENGTH_SHORT).show();
        }
    }

    private void SwipOnScreen(double XStart,double YStart,double XEnd,double YEnd)
    {
        try
        {

            RootOutputStream.writeBytes("input swipe "+XStart+" "+YStart+" "+XEnd+" "+YEnd+"\n");

            RootOutputStream.flush();

            RootOutputStream.writeBytes("exit\n");

            RootOutputStream.flush();

        }
        catch(IOException E)
        {
            Toast.makeText(MyContext,"Error while writing command",Toast.LENGTH_SHORT).show();
        }
    }

    private void FindMiddleOrIndex(ArrayList<Finger> Fingers)
    {

        for(int i=0 ; i < Fingers.size() ; i++)
        {
            if(Fingers.get(i).GetFingerName().equals("Index"))
            {
                IndexFound=true;
                Index=Fingers.get(i);
            }
            else
            {
                if(Fingers.get(i).GetFingerName().equals("Middle"))
                {
                    MiddleFound=true;
                    Middle=Fingers.get(i);
                }
            }
            if(IndexFound && MiddleFound)
                return;
        }
    }

    private void ProcessTap(int CircleRadius,int FrameCount)
    {
        if(FrameCount < 16 * SecondToWait)
        {
            FirstIndexPoint.set((int)Index.GetFingerTipPoint().x,(int)Index.GetFingerTipPoint().y);
        }
        else
        {
            if(FrameCount == 16 * SecondToWait)
            {
                ClickOnScreen(Index.GetFingerTipPoint().x,Index.GetFingerTipPoint().y);
                HandProcessor.FrameCounts = 0;
            }
            else
            {
                if(!IsInsideCircle(CircleRadius))
                {

                    HandProcessor.FrameCounts = 0;

                }

                else

                {

                    HandProcessor.FrameCounts ++;

                }
            }
        }
    }

    private void ProcessSwip()
    {
        org.opencv.core.Point startPoint = null;

        org.opencv.core.Point endPoint;

       if(swipFrameCount == 1)

           startPoint = new org.opencv.core.Point(Index.GetFingerTipPoint().x, Index.GetFingerTipPoint().y);

       if(swipFrameCount < 16 * SecondToWait )

           swipFrameCount ++ ;

       else

           if(swipFrameCount == 16 * SecondToWait) {

               endPoint = new org.opencv.core.Point(Index.GetFingerTipPoint().x, Index.GetFingerTipPoint().y);

               SwipOnScreen( startPoint.x , startPoint.y , endPoint.x , endPoint.y );

               swipFrameCount = 0;
           }


    }

    /**
     * Method to check wether the finger is inside the epsilon radius or not
     * @param radius Epsilon radius
     * @return True if the finger inside the epsilon circle
     * false otherwise
     */
    private boolean IsInsideCircle(int radius)
    {
        return ((Math.pow((Index.GetFingerTipPoint().x - FirstIndexPoint.x),2)
                + Math.pow((Index.GetFingerTipPoint().y-FirstIndexPoint.y),2)
                - radius) <= 0);
    }
}
