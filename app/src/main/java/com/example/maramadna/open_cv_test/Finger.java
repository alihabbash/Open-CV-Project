package com.example.maramadna.open_cv_test;
import org.opencv.core.Point;

/**
 * Created by X_PC on 7/30/2017.
 */
public class Finger {
    private String FingerName;
    private Point FingerTipPoint;
    private double FingerLenght;

    public Finger(double Lenght)
    {
        FingerName="";
        FingerTipPoint = new Point(0,0);
        FingerLenght = Lenght;
    }

    public void SetFingerName(String fingerName)
    {
        FingerName=fingerName;
    }

    public void SetFingerTipPoint(double x , double y)
    {
        FingerTipPoint.x = x;
        FingerTipPoint.y = y;
    }

//    public void SetFingerLenght(double Lenght)
//    {
//        FingerLenght=Lenght;
//        SetFingerName();
//    }

    public double GetFingerLenght()
    {
        return FingerLenght;
    }

    public String GetFingerName()
    {
        return FingerName;
    }

    public Point GetFingerTipPoint()
    {
        return FingerTipPoint;
    }


}
